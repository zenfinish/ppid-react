import React, { Component } from 'react';
import { HashRouter as Router, Route, Switch } from 'react-router-dom';
import 'semantic-ui-css/semantic.min.css';
import Header from './Header.jsx';
import Footer from './Footer.jsx';

import Home from './Home.jsx';
import Login from './views/Login.jsx';
import Register from './views/Register.jsx';
import Profil from './views/Profil.jsx';
import TugasWewenang from './views/TugasWewenang.jsx';
import StrukturPpid from './views/StrukturPpid.jsx';
import VisiMisi from './views/VisiMisi.jsx';
import Aktivasi from './views/Aktivasi.jsx';
import Page404 from './views/Page404.jsx';

import MemberDashboard from './views/member/Dashboard.jsx';
import MemberProfil from './views/member/Profil.jsx';

class App extends Component {
	
	render() {
		return (
			<Router basename={process.env.REACT_APP_BASENAME}>
				<Header />

				<Switch>
					<Route exact path={`${process.env.PUBLIC_URL}/`} component={Home} />
					<Route path={`${process.env.PUBLIC_URL}/login`} component={Login} />
					<Route path={`${process.env.PUBLIC_URL}/profil`} component={Profil} />
					<Route path={`${process.env.PUBLIC_URL}/register`} component={Register} />
					<Route path={`${process.env.PUBLIC_URL}/tugas-wewenang`} component={TugasWewenang} />
					<Route path={`${process.env.PUBLIC_URL}/struktur-ppid`} component={StrukturPpid} />
					<Route path={`${process.env.PUBLIC_URL}/visi-misi`} component={VisiMisi} />
					<Route path={`${process.env.PUBLIC_URL}/aktivasi/:token`} component={Aktivasi} />
					<Route path={`${process.env.PUBLIC_URL}/404`} component={Page404} />
					
					<Route path={`${process.env.PUBLIC_URL}/member/dashboard`} component={MemberDashboard} />
					<Route path={`${process.env.PUBLIC_URL}/member/profil`} component={MemberProfil} />
				</Switch>

				<Footer />
			</Router>
		)
	}
}

export default App;