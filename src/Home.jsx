import React, { Component, Fragment } from 'react';
import { Grid, Image, Segment, Input, Icon, Header, Divider, Pagination, Button, Label } from 'semantic-ui-react';
import api from './configs/api.js';

class Home extends Component {

	state = {
		loading: false,
		dataInformasi: [],
	}
	
	componentDidMount() {
		this.fetchInformasi();
	}

	fetchInformasi = () => {
		api.get(`/content/dokumen/limit6`)
		.then(result => {
			this.setState({ dataInformasi: result.data });
		})
		.catch(error => {
			console.log(error.response)
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				
				<Segment style={{ textAlign: 'left' }}>
					<Grid style={{ padding: 20 }}>
						<Grid.Column width={14}>
							<h3>Cari Informasi Publik ?</h3>
							<div>Salah satu kewajiban badan publik yang dinyatakan dalam Undang-Undang No 14 Tahun 2008 adalah menyediakan Daftar Informasi Publik (DIP). DIP adalah catatan yang berisi keterangan sistematis tentang informasi publik yang berada dibawah penguasaan badan publik. Melalui aplikasi PPID Kemendagri yang digunakan ini, badan publik dapat mempublikasi informasi yang dikuasai yang selanjutnya tersusun sebagai DIP secara otomatis.</div><br />
						</Grid.Column>
						<Grid.Column width={2}>
							<Image src={require('./img/LOGO_KOTA_BANDAR_LAMPUNG_BARU.png')} />
						</Grid.Column>
					</Grid>
					<Input icon='search' placeholder='silahkan ketik informasi publik yang kamu cari !' fluid style={{ marginBottom: 5 }} />
					<Button fluid as="a" href="Dokumentasi Aplikasi PPID Kota Bandar Lampung.doc" target="_blank">Download Dokumentasi Tentang Aplikasi PPID Disini.</Button>
				</Segment>

				<Grid container stackable verticalAlign='middle' style={{ marginTop: 10 }}>
					<Grid.Row>
						<Grid.Column width={5}>
							<Segment raised style={{ padding: '1em' }} color='red'>
								<Grid>
									<Grid.Column width={4}>
										<Segment inverted color='red' textAlign='center'>
											<Icon name='copy outline' />
										</Segment>
									</Grid.Column>
									<Grid.Column width={12}>
										<Header>Total Informasi<br />0</Header>
									</Grid.Column>
								</Grid>
							</Segment>
						</Grid.Column>
						<Grid.Column width={6}>
							<Segment raised style={{ padding: '1em' }} color='yellow'>
								<Grid>
									<Grid.Column width={3}>
										<Segment inverted color='yellow' textAlign='center'>
											<Icon name='sitemap' />
										</Segment>
									</Grid.Column>
									<Grid.Column width={13}>
										<Header>Total Satuan Kerja<br />0</Header>
									</Grid.Column>
								</Grid>
							</Segment>
						</Grid.Column>
						<Grid.Column width={5}>
							<Segment raised style={{ padding: '1em' }} color='green'>
								<Grid>
									<Grid.Column width={4}>
										<Segment inverted color='green' textAlign='center'>
											<Icon name='group' />
										</Segment>
									</Grid.Column>
									<Grid.Column width={12}>
										<Header>Total Group<br />0</Header>
									</Grid.Column>
								</Grid>
							</Segment>
						</Grid.Column>
					</Grid.Row>
				</Grid>

				<Grid container stackable verticalAlign='middle' style={{ marginTop: 10 }}>
					<Grid.Row>
						<Grid.Column width={16}>
							<Segment raised style={{ padding: '1em' }}>
								<Header as="h2">Informasi Sektoral (Group)</Header>
								<Divider />
								<Grid>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/team-work-background-in-flat-style_23-2147762182.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'blue',
												content: 'Administrasi Pemerintahan',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/financial-flowchart_1284-1145.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'blue',
												content: 'Ekonomi dan Keuangan',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/vector-line-art-abstract-illustration-smart-home-controlling-through-internet-home-work-equipment_1441-217.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'black',
												content: 'Infrastuktur',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/people-background-design_23-2147676916.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'yellow',
												content: 'Kependudukan',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
								</Grid>
								<Grid>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/medicine-elements-background-in-flat-style_23-2147769343.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'blue',
												content: 'Kesehatan',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/earth-day-concept-human-hands-holding-globe_3446-182.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'green',
												content: 'Lingkungan Hidup',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/education-design-with-icons_23-2147501091.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'blue',
												content: 'Pendidikan',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
									<Grid.Column width={4}>
										<Image
											src='https://image.freepik.com/free-vector/business-team-starting-up-work-together_3446-586.jpg'
											size='massive'
											href='#'
											rounded
											label={{
												color: 'yellow',
												content: 'Sosial',
												ribbon: true,
												style: { fontSize: 12 }
											}}
											centered
											style={{ cursor: 'pointer' }}
										/>
									</Grid.Column>
								</Grid>
							</Segment>
						</Grid.Column>
					</Grid.Row>
				</Grid>

				<Grid container stackable verticalAlign='middle'>
					<Grid.Row>
						<Grid.Column width={16}>
							<Segment raised style={{ padding: '1em' }}>
								<Header as="h2">Informasi Terbaru</Header>
								{
									this.state.dataInformasi.map((item, index) => (
										<Fragment key={index}>
											<Segment color="blue" basic style={{ marginBottom: 30 }}>
												<h3 style={{ color: '#00008B' }}>
													<Icon name={
														item.type === 'application/pdf' ? 'file pdf outline'
														: item.type === 'image/jpeg' ? 'file image outline'
														: 'file'
													}/>
													{item.nama}
												</h3>
												<div style={{ color: 'grey', marginBottom: 3 }}>{item.grup_nama} - {item.skpd_nama}</div>
												<div style={{ float: 'left' }}>
													<a href=""><Label color="blue"><Icon name="download" /> Download</Label></a>
												</div>
												<div style={{ float: 'right' }}>
													<i style={{ fontSize: 12, color: 'grey'}}>{item.type} - {item.tgl}</i>
												</div>
											</Segment>
										</Fragment>
									))
								}
								<Pagination
									defaultActivePage={5}
									ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
									firstItem={{ content: <Icon name='angle double left' />, icon: true }}
									lastItem={{ content: <Icon name='angle double right' />, icon: true }}
									prevItem={{ content: <Icon name='angle left' />, icon: true }}
									nextItem={{ content: <Icon name='angle right' />, icon: true }}
									totalPages={10}
								/>
							</Segment>
						</Grid.Column>
					</Grid.Row>
				</Grid>

			</Segment>
		)
	}
}

export default Home;