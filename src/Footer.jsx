import React, { Component } from 'react';
import { Container, Grid, Header, List, Segment, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

class Footer extends Component {

	render() {
		return (
			<Segment inverted vertical style={{ padding: '5em 0em' }}>
				<Container>
					<Grid divided inverted stackable>
						<Grid.Row>
							<Grid.Column width={4}>
								<Header inverted as='h4' content='Mitra' />
								<List link inverted>
									<List.Item
										as='a'
										href="http://bandarlampungkota.go.id/"
										target="_blank"
									>Pemerintah Kota Bandar Lampung</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column width={4}>
								<Header inverted as='h4' content='Tentang' />
								<List link inverted>
									<List.Item as={Link} to="/profil"><Icon name="building outline" /> Profil</List.Item>
									<List.Item as={Link} to="/tugas-wewenang"><Icon name="address card outline" /> Tugas & Wewenang</List.Item>
									<List.Item as={Link} to="/struktur-ppid"><Icon name="sitemap" /> Struktur PPID</List.Item>
									<List.Item as={Link} to="/visi-misi"><Icon name="target" /> Visi & Misi</List.Item>
								</List>
							</Grid.Column>
							<Grid.Column width={8}>
								<Header inverted as='h4' content='Kontak' />
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Container>
			</Segment>
		)
	}
}

export default Footer