import React, { Component } from 'react';
import { Segment, Grid, Divider, Form, Button, Message } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import api from '../configs/api.js';

class Login extends Component {

	state = {
		loading: false,
		email: '',
		password: '',

		loadingLogin: false,

		openMessage: 'none',
		headerMessage: '',
		pMessage: '',
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}

	login = (e) => {
		e.preventDefault();
		if (this.state.email === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Email masih kosong',
			});
		} else if (this.state.password === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Password masih kosong',
			});
		} else {
			this.setState({
				openMessage: 'none',
				headerMessage: '',
				pMessage: '',
				loadingLogin: true,
			}, () => {
				api.post(`/people/login`, {
					email: this.state.email,
					password: this.state.password,
				})
				.then(result => {
					localStorage.setItem('token', result.data);
					this.props.history.push('/member/dashboard');
					window.location.reload();
				})
				.catch(error => {
					this.setState({
						loadingLogin: false,
						openMessage: '',
						headerMessage: 'Maaf.',
						pMessage: JSON.stringify(error.response),
					});
				});
			});
		}
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid columns={2} relaxed='very' stackable>
					<Grid.Column>
						<h3>Sudah Punya Akun ?</h3>
						<div>Silahkan masukan email dan password Anda</div><br/>
						<Form onSubmit={this.login}>
							<Form.Field>
								<label>Email</label>
								<input placeholder='Masukkan Email Anda' type="text" onChange={(e) => {this.setState({ email: e.target.value })}} />
							</Form.Field>
							<Form.Field>
								<label>Password</label>
								<input placeholder='Password' type="password" autoComplete="off" onChange={(e) => {this.setState({ password: e.target.value })}} />
							</Form.Field>
							<Form.Field>
								<Button loading={this.state.loadingLogin}>Masuk</Button>
							</Form.Field>
							<Form.Field>
								<Message negative style={{ display: this.state.openMessage }}>
									<Message.Header>{this.state.headerMessage}</Message.Header>
									<p>{this.state.pMessage}</p>
								</Message>
							</Form.Field>
						</Form>
					</Grid.Column>

					<Grid.Column verticalAlign='middle'>
						<h3>Selamat datang di SIP PPID</h3>
						<div>Jika anda belum memiliki akun silahkan daftar terlebih dahulu agar anda dapat menggunakan semua fitur-fitur yang ada di sini dengan baik dan bijak.</div><br/>
						<Button as={Link} to="/register">Daftar Akun</Button>
					</Grid.Column>
				</Grid>
				<Divider vertical>Or</Divider>
			</Segment>
		)
	}
}

export default Login