import React, { Component } from 'react';
import { Segment, Grid } from 'semantic-ui-react';

class VisiMisi extends Component {

	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<Segment raised style={{ padding: '1em' }}>
							<h3>VISI</h3>
							<p>Terwujudnya sistem politik yang demokratis, pemerintah yang desentralistrik, pembangunan daerah yang berkelanjutan, serta keberdayaan masyarakat yang partisipatif dengan didukung sumber daya aparatur yang profesional dalam wadah Negara Kesatuan Republik Indonesia.</p>
							<h3>MISI</h3>
							<p>Menetapkan Kebijaksanaan Nasional dan memfasilitasi penyelenggara pemerintah dalam upaya :</p>
							<ul>
								<li>Memperkuat Keutuhan NKRI serta memantapkan sistem politik dalam negeri yang demokratis</li>
								<li>Memantapkan penyelenggaraan tugas-tugas pemerintah umum</li>
								<li>Memantapkan efektifitas dan efisiensi penyelenggaraan pemerintahan yang desentralistik</li>
								<li>Mengembangkan keserasian hubungan pusat-daerah, antara daerah dan antar kawasan secara kemandirian daerah dalam pengelolaan pembangunan secara berkelanjutan</li>
								<li>Memperkuat otonomi desa dan meningkatkan keberdayaan masyarakat dalam aspek ekonomi, sosial dan budaya</li>
								<li>Mewujudkan tata pemerintahan yang baik, bersih dan berwibawa</li>
							</ul>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default VisiMisi