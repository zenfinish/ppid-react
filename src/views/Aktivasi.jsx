import React, { Component } from 'react';
import { Segment, Grid, Divider } from 'semantic-ui-react';
import api from '../configs/api.js';

class Aktivasi extends Component {

	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			api.get(`/people/aktivasi/${this.props.match.params.token}`, {
				nama: this.state.nama,
				nik: this.state.nik,
				email: this.state.email,
				password: this.state.password,
				ulangPassword: this.state.ulangPassword,
				alamat: this.state.alamat,
				hp: this.state.hp,
				propinsi: this.state.propinsi,
				kota: this.state.kota,
			})
			.then(result => {
				this.setState({ loading: false });
			})
			.catch(error => {
				this.props.history.push('/404');
			});
			// this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<Segment raised style={{ padding: '1em' }}>
							<h3>Akun Anda telah aktif sekarang!</h3>
							<Divider></Divider>
							<h1>Selamat Datang PPID Kota Bandar Lampung</h1>
							<p>Akun Anda telah diaktifkan. Anda dapat melakukan log in kedalam website menggunakan email yang Anda daftarkan.</p>
							<p>Terimakasih,<br />Tim PPID Kota Bandar Lampung</p>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default Aktivasi