import React, { Component } from 'react';
import { Segment, Grid, Accordion, Icon, Image, Form, Button, Message } from 'semantic-ui-react';
import api from '../configs/api.js';

class Register extends Component {

	state = {
		loading: false,
		activeIndex: 0,

		nama: '',
		nik: '',
		email: '',
		password: '',
		ulangPassword: '',
		alamat: '',
		hp: '',
		propinsi: '',
		kota: '',

		openMessage: 'none',
		headerMessage: '',
		pMessage: '',

		messagePhoto: 'none',
		messagePhotoHeader: '',
		messagePhotoP: '',

		loadingDaftar: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}

	handleClick = (e, titleProps) => {
		const { index } = titleProps
		const { activeIndex } = this.state
		const newIndex = activeIndex === index ? -1 : index
  
		this.setState({ activeIndex: newIndex })
	}

	daftar = (e) => {
		e.preventDefault();
		if (this.state.nama === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Nama Lengkap masih kosong',
			});
		} else if (this.state.nik === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'NIK (Nomor Induk Kependudukan) masih kosong',
			});
		} else if (this.state.alamat === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Alamat masih kosong',
			});
		} else if (this.state.propinsi === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Propinsi masih kosong',
			});
		} else if (this.state.kota === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Kota atau Kabupaten masih kosong',
			});
		} else if (this.state.hp === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'No. Kontak / Handphone masih kosong',
			});
		} else if (this.state.email === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Email Aktif masih kosong',
			});
		} else if (this.state.password === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Password masih kosong',
			});
		} else if (this.state.ulangPassword === '') {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Konfirmasi Password masih kosong',
			});
		} else if (this.state.ulangPassword !== this.state.password) {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Password dan Konfirmasi Password tidak sama',
			});
		} else {
			this.setState({
				openMessage: 'none',
				headerMessage: '',
				pMessage: '',
				loadingDaftar: true,
			}, () => {
				api.post(`/people/register`, {
					nama: this.state.nama,
					nik: this.state.nik,
					email: this.state.email,
					password: this.state.password,
					ulangPassword: this.state.ulangPassword,
					alamat: this.state.alamat,
					hp: this.state.hp,
					propinsi: this.state.propinsi,
					kota: this.state.kota,
				})
				.then(result => {
					this.setState({ loadingDaftar: false });
				})
				.catch(error => {
					this.setState({
						loadingDaftar: false,
						openMessage: '',
						headerMessage: 'Maaf.',
						pMessage: JSON.stringify(error.response),
					});
				});
			});
		}
	}
	
	render() {
		const { activeIndex } = this.state;
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={8}>
						<Segment raised style={{ padding: '1em' }}>
							<h3>Pendaftaran</h3>
							<div>Silahkan masukan informasi Anda</div><br/>
							<Form onSubmit={this.daftar}>
								<Form.Field>
									<label>Nama Lengkap</label>
									<input type="text" onChange={(e) => {this.setState({ nama: e.target.value })}} value={this.state.nama} />
								</Form.Field>
								<Form.Field>
									<label>NIK (Nomor Induk Kependudukan)</label>
									<input type="text" onChange={(e) => {this.setState({ nik: e.target.value })}} value={this.state.nik} />
								</Form.Field>
								<Form.Field>
									<label>Alamat</label>
									<textarea onChange={(e) => {this.setState({ alamat: e.target.value })}} value={this.state.alamat} />
								</Form.Field>
								<Form.Field>
									<label>Propinsi</label>
									<input type="text" onChange={(e) => {this.setState({ propinsi: e.target.value })}} value={this.state.propinsi} />
								</Form.Field>
								<Form.Field>
									<label>Kota atau Kabupaten</label>
									<input type="text" onChange={(e) => {this.setState({ kota: e.target.value })}} value={this.state.kota} />
								</Form.Field>
								<Form.Field>
									<label>No. Kontak / Handphone</label>
									<input type="text" onChange={(e) => {this.setState({ hp: e.target.value })}} value={this.state.hp} />
								</Form.Field>
								<Segment inverted color="blue">Untuk dapat menggunakan fitur PPID, Anda harus memiliki Email Aktif dan Password. Silakan tentukan Email Aktif dan password Anda</Segment>
								<Form.Field>
									<label>Email Aktif</label>
									<input type="text" onChange={(e) => {this.setState({ email: e.target.value })}} value={this.state.email} />
								</Form.Field>
								<Form.Field>
									<label>Password</label>
									<input type="password" onChange={(e) => {this.setState({ password: e.target.value })}} value={this.state.password} autoComplete="off" />
								</Form.Field>
								<Form.Field>
									<label>Konfirmasi Password</label>
									<input type="password" onChange={(e) => {this.setState({ ulangPassword: e.target.value })}} value={this.state.ulangPassword} autoComplete="off" />
								</Form.Field>
								<Form.Field>
									<Button loading={this.state.loadingDaftar}>Daftarkan Akun</Button>
								</Form.Field>
								<Form.Field>
									<Message negative style={{ display: this.state.openMessage }}>
										<Message.Header>{this.state.headerMessage}</Message.Header>
										<p>{this.state.pMessage}</p>
									</Message>
								</Form.Field>
							</Form>
						</Segment>
					</Grid.Column>
					<Grid.Column width={8}>
						<div>
							<Image src={require('../img/user.png')} size='mini' circular verticalAlign='middle' style={{ marginRight: 10 }} />
							<span style={{ fontSize: 20 }}>Ketentuan Pengguna</span>
						</div><br/>
						<Accordion styled>
							<Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
								<Icon name='dropdown' /> Umum
							</Accordion.Title>
							<Accordion.Content active={activeIndex === 0}>
								<table>
									<tbody>
										<tr>
											<td valign="top">1. </td>
											<td>Pengguna adalah siapa saja yang memanfaatkan layanan SIP PPID baik sebagai individu maupun lembaga.</td>
										</tr>
										<tr>
											<td valign="top">2. </td>
											<td>Dengan menggunakan Layanan ini Pengguna menyetujui sepenuhnya Persyaratan dan Ketentuan Layanan yang diuraikan di dalam dokumen ini. Jika Pengguna tidak menyetujui Persyaratan dan Ketentuan Layanan ini, harap jangan gunakan Layanan ini.</td>
										</tr>
									</tbody>
								</table>
							</Accordion.Content>

							<Accordion.Title active={activeIndex === 1} index={1} onClick={this.handleClick}>
								<Icon name='dropdown' /> Ketentuan Pengguna
							</Accordion.Title>
							<Accordion.Content active={activeIndex === 1}>
								<p>Seluruh sumber data dan informasi yang disimpan/direkam/diolah pada Layanan ini termasuk validasi dan verifikasi keabsahannya sepenuhnya menjadi tanggung jawab dari Pengguna Layanan.</p>
								<p>Operasional pengelolaan seluruh data dan informasi mencakup pemutakhiran data dan informasi dimaksud sepenuhnya menjadi tanggung jawab dari Pengguna sesuai hak akses masing-masing.</p>
								<p>Pengguna dilarang mendistribusikan data dan informasi dari Layanan ini kepada pihak ketiga kecuali telah mendapat ijin resmi dan tertulis dari Pengelola.</p>
								<p>Pengguna berkewajiban memahami dan mematuhi Kebijakan Privasi Data dan Informasi dan Kebijakan Keamanan yang ditetapkan oleh pengelola.</p>
							</Accordion.Content>

							<Accordion.Title active={activeIndex === 2} index={2} onClick={this.handleClick}>
								<Icon name='dropdown' /> Kebijakan Privasi Data & Informasi
							</Accordion.Title>
							<Accordion.Content active={activeIndex === 2}>
								<p>Pengelola akan mencatat informasi tentang semua aktifitas dari pengguna. Mencakup semua transaksi data dan informasi dari atau antar pengguna. Pengelola juga dapat mengumpulkan data dan informasi tentang pengguna dari pengguna lain. Hal ini untuk memberikan pengalaman yang lebih baik dalam Layanan untuk para pengguna.</p>
								<p>Penggunaan Layanan dan isinya disediakan dan dikembangkan oleh Pengelola secara berkesinambungan untuk kemudahan pengelolaan data dan informasi sesuai kepentingan dan kebutuhan Pengguna. Oleh sebab itu, pengelolaan dan pemanfaatan data dan informasi yang tersedia merupakan sepenuhnya tanggung jawab dari Pengguna. Tersedia apa adanya dan sebagaimana tersedia, tanpa jaminan jenis apapun dari Pengelola.</p>
								<p>Pengelola tidak akan dikenakan tanggung jawab atas kerusakan langsung, tidak langsung, tidak disengaja, khusus atau secara konsekuensi, kerugian atau gangguan yang timbul dari penggunaan atau kesalahan informasi yang diberikan oleh Pengguna.</p>
								<p>Pengguna memahami sepenuhnya terhadap privasi data dan informasi yang mereka kelola. Pemanfaatan data dan informasi serta fasilitas pada Layanan ini sepenuhnya merupakan tanggung jawab pengguna.</p>
							</Accordion.Content>

							<Accordion.Title active={activeIndex === 3} index={3} onClick={this.handleClick}>
								<Icon name='dropdown' /> Ketentuan Penggunaan Akun
							</Accordion.Title>
							<Accordion.Content active={activeIndex === 3}>
								<p>Untuk menggunakan Layanan, Pengguna akan diberikan sebuah akun tertentu sesuai dengan prosedur yang berlaku. Pengguna dilarang menggunakan akun milik pihak lain tanpa izin.</p>
								<p>Pengelola akan mengakhiri akses akun Pengguna Layanan bila menurut pertimbangan Pengelola bahwa Pengguna telah melanggar Persyaratan dan Ketentuan Layanan.</p>
								<p>Saat melakukan proses permintaan dan aktivasi akun, Pengguna harus memberikan informasi yang lengkap dan akurat. Pengguna secara sendiri bertanggung jawab atas aktivitas yang dilakukan akun miliknya, dan berkewajiban mengamankan kata sandi yang dimilikinya.</p>
								<p>Pengguna berkewajiban segera melaporkan Pengelola apabila terjadi penyimpangan keamanan atau penggunaan tanpa izin atas akun miliknya.</p>
								<p>Pengelola tidak dapat dimintakan pertanggungjawaban atas kerugian akibat penggunaan akun Pengguna oleh pihak lain. Pengguna bertanggung jawab penuh atas kerugian akibat penggunaan akun Pengguna oleh pihak lain.</p>
							</Accordion.Content>

							<Accordion.Title active={activeIndex === 4} index={4} onClick={this.handleClick}>
								<Icon name='dropdown' /> Perubahan Aturan
							</Accordion.Title>
							<Accordion.Content active={activeIndex === 4}>
								<p>Kebijakan Pengelola memainkan peranan penting dalam mempertahankan pengalaman positif bagi Pengguna. Harap patuhi kebijakan tersebut saat menggunakan Layanan ini. Saat Pengelola diberi tahu tentang kemungkinan pelanggaran kebijakan, kami dapat meninjau dan mengambil tindakan, termasuk membatasi atau menghentikan akses pengguna ke Layanan ini.</p>
								<p>Pengelola dapat memperbaiki, menambah, atau mengurangi ketentuan ini setiap saat, dengan atau tanpa pemberitahuan sebelumnya. Pengguna diharapkan memantau Persyaratan dan Ketentuan Layanan ini sewaktu-waktu. Seluruh Pengguna terikat dan tunduk kepada ketentuan yang telah diperbaiki/ditambah/dikurangi oleh Pengelola.</p>
							</Accordion.Content>
						</Accordion>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default Register