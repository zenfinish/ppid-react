import React, { Component } from 'react';
import { Segment, Grid } from 'semantic-ui-react';

class StrukturPpid extends Component {

	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<Segment raised style={{ padding: '1em' }}>
							INI HALAMAN STRUKTUR PPID
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default StrukturPpid