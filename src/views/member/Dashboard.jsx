import React, { Component } from 'react';
import { Segment, Grid, Divider, Button, Icon, Image, Header } from 'semantic-ui-react';
import { Route, Switch, Link } from 'react-router-dom';
import api from '../../configs/api.js';

import AjukanPermohonan from './AjukanPermohonan.jsx';
import PermohonanSaya from './PermohonanSaya.jsx';
import KeberatanSaya from './KeberatanSaya.jsx';
import DokumenSaya from './DokumenSaya.jsx';

class Dashboard extends Component {
	
	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			api.get(`/people/cek-token`)
			.then(result => {
				this.setState({ loading: false });
			})
			.catch(error => {
				this.setState({ token: false }, () => {
					localStorage.removeItem('token');
					this.props.history.push('/');
				});
			});
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid columns={2} divided>
					<h3>Informasi Pengguna</h3>
					<Divider />
					<Grid.Row>
						<Grid.Column width={5}>
							<Grid columns={5}>
								<Image src={require('../../img/user.png')} size="small" circular />
							</Grid>
							<Grid columns={11}>
							<Header as='h4'>
								<Header.Content>Zainal Abidin</Header.Content>
							</Header>
							<Header as='h4'>
								<Header.Content>
									<Header.Subheader>Terakhir Login</Header.Subheader>
									11 October 2019 15:59
								</Header.Content>
							</Header>
							</Grid>
						</Grid.Column>
					</Grid.Row>
					<Grid.Column verticalAlign='middle' width={11}>
						<h3>Fasilitas Saya</h3>
						<Divider />
						<Grid columns={5}>
							<Grid.Column style={{ textAlign: 'center' }}>
								<Button icon size="huge" color="yellow" as={Link} to="/member/dashboard/">
									<Icon name='dashboard' size="huge" />
								</Button>
								<h4>Dashboard</h4>
							</Grid.Column>
							<Grid.Column style={{ textAlign: 'center' }}>
								<Button icon size="huge" color="blue" as={Link} to="/member/dashboard/ajukan-permohonan">
									<Icon name='clipboard list' size="huge" />
								</Button>
								<h4>Ajukan Permohonan</h4>
							</Grid.Column>
							<Grid.Column style={{ textAlign: 'center' }}>
								<Button icon size="huge" color="red" as={Link} to="/member/dashboard/permohonan-saya">
									<Icon name='envelope open outline' size="huge" />
								</Button>
								<h4>Permohonan Saya</h4>
							</Grid.Column>
							<Grid.Column style={{ textAlign: 'center' }}>
								<Button icon size="huge" color="green" as={Link} to="/member/dashboard/keberatan-saya">
									<Icon name='frown outline' size="huge" />
								</Button>
								<h4>Keberatan Saya</h4>
							</Grid.Column>
							<Grid.Column style={{ textAlign: 'center' }}>
								<Button icon size="huge" color="pink" as={Link} to="/member/dashboard/dokumen-saya">
									<Icon name='archive' size="huge" />
								</Button>
								<h4>Dokumen Saya</h4>
							</Grid.Column>
						</Grid>
					</Grid.Column>
				</Grid>
				<Divider />
				<Segment basic>
					<Switch>
						<Route path={`${process.env.PUBLIC_URL}/member/dashboard/ajukan-permohonan`} component={AjukanPermohonan} />
						<Route path={`${process.env.PUBLIC_URL}/member/dashboard/permohonan-saya`} component={PermohonanSaya} />
						<Route path={`${process.env.PUBLIC_URL}/member/dashboard/keberatan-saya`} component={KeberatanSaya} />
						<Route path={`${process.env.PUBLIC_URL}/member/dashboard/dokumen-saya`} component={DokumenSaya} />
					</Switch>
				</Segment>
			</Segment>
		)
	}
}

export default Dashboard