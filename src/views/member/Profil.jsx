import React, { Component } from 'react';
import { Segment, Grid, Image, Message, Divider, Icon } from 'semantic-ui-react';
import api from '../../configs/api.js';

const imgSrc = 'https://react.semantic-ui.com/images/wireframe/square-image.png';

class Profil extends Component {

	state = {
		loading: false,
		activeIndex: 0,

		openMessage: 'none',
		headerMessage: '',
		pMessage: '',

		messagePhoto: 'none',
		messagePhotoHeader: '',
		messagePhotoP: '',

		loadingDaftar: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}

	handleClick = (e, titleProps) => {
		const { index } = titleProps
		const { activeIndex } = this.state
		const newIndex = activeIndex === index ? -1 : index
  
		this.setState({ activeIndex: newIndex })
	}

	daftar = (e) => {
		e.preventDefault();
		if (this.state.ulangPassword !== this.state.password) {
			this.setState({
				openMessage: '',
				headerMessage: 'Maaf.',
				pMessage: 'Password dan Konfirmasi Password tidak sama',
			});
		} else {
			this.setState({
				openMessage: 'none',
				headerMessage: '',
				pMessage: '',
				loadingDaftar: true,
			}, () => {
				api.post(`/people/Profil`, {
					nama: this.state.nama,
					nik: this.state.nik,
					email: this.state.email,
					password: this.state.password,
					ulangPassword: this.state.ulangPassword,
					alamat: this.state.alamat,
					hp: this.state.hp,
					propinsi: this.state.propinsi,
					kota: this.state.kota,
				})
				.then(result => {
					this.setState({ loadingDaftar: false });
				})
				.catch(error => {
					console.log(error.response);
					this.setState({ loadingDaftar: false });
				});
			});
		}
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<div>
						<h3>Akun Saya</h3>
						<p>Data yang sudah didaftarkan tidak dapat diubah, Anda hanya bisa mengubah foto akun untuk identifikasi wajah pemilik akun dan password.</p>
					</div><br/><br/><br/><br/>
					<Grid.Column width={5}>
						<Segment raised style={{ padding: '1em' }}>
							<Image src={imgSrc} circular size="medium" id='output' centered />
							<Divider></Divider>
							<label htmlFor="foto" style={{
								cursor: 'pointer',
								backgroundColor: 'yellow',
								padding: 5
							}}>Ganti Foto</label>
							<input
								type="file"
								id="foto"
								style={{ display: 'none' }}
								onChange={
									(e) => {
										if (e.target.files.length !== 0) {
											if (e.target.files[0].type !== 'image/jpeg') {
												this.setState({
													messagePhoto: '',
													messagePhotoHeader: 'Maaf.',
													messagePhotoP: 'type data yang diizinkan hanya image/jpeg',
												});
												let output = document.getElementById('output');
												output.src = imgSrc;
											} else if (e.target.files[0].size > 2000000) {
												this.setState({
													messagePhoto: '',
													messagePhotoHeader: 'Maaf.',
													messagePhotoP: 'ukuran maximum yang diizinkan hanya 2MB',
												});
												let output = document.getElementById('output');
												output.src = imgSrc;
											} else {
												let reader = new FileReader();
												reader.onload = () => {
													let dataURL = reader.result;
													let output = document.getElementById('output');
													output.src = dataURL;
													this.setState({
														messagePhoto: 'none',
														messagePhotoHeader: '',
														messagePhotoP: '',
													})
												};
												reader.readAsDataURL(e.target.files[0]);
											}
										}
									}
								}
							/>
							<Message negative style={{ display: this.state.messagePhoto }}>
								<Message.Header>{this.state.messagePhotoHeader}</Message.Header>
								<p>{this.state.messagePhotoP}</p>
							</Message>
							<div><u>Batas maksimal upload foto adalah <b>2MB</b></u></div>
						</Segment>
						<Segment raised style={{ padding: '1em' }}>
							<Image src={imgSrc} rounded size="medium" id='output' centered />
							<Divider></Divider>
							<label htmlFor="ktp" style={{
								cursor: 'pointer',
								backgroundColor: 'yellow',
								padding: 5
							}}>Ganti KTP</label>
							<input
								type="file"
								id="ktp"
								style={{ display: 'none' }}
								onChange={
									(e) => {
										if (e.target.files.length !== 0) {
											if (e.target.files[0].type !== 'image/jpeg') {
												this.setState({
													messagePhoto: '',
													messagePhotoHeader: 'Maaf.',
													messagePhotoP: 'type data yang diizinkan hanya image/jpeg',
												});
												let output = document.getElementById('output');
												output.src = imgSrc;
											} else if (e.target.files[0].size > 2000000) {
												this.setState({
													messagePhoto: '',
													messagePhotoHeader: 'Maaf.',
													messagePhotoP: 'ukuran maximum yang diizinkan hanya 2MB',
												});
												let output = document.getElementById('output');
												output.src = imgSrc;
											} else {
												let reader = new FileReader();
												reader.onload = () => {
													let dataURL = reader.result;
													let output = document.getElementById('output');
													output.src = dataURL;
													this.setState({
														messagePhoto: 'none',
														messagePhotoHeader: '',
														messagePhotoP: '',
													})
												};
												reader.readAsDataURL(e.target.files[0]);
											}
										}
									}
								}
							/>
							<Message negative style={{ display: this.state.messagePhoto }}>
								<Message.Header>{this.state.messagePhotoHeader}</Message.Header>
								<p>{this.state.messagePhotoP}</p>
							</Message>
							<div><u>Batas maksimal upload foto adalah <b>2MB</b></u></div>
						</Segment>
					</Grid.Column>
					<Grid.Column width={11}>
						<Segment raised style={{ padding: '1em' }}>
							<h3><Icon name="user" /> Informasi Pribadi</h3>
							<Divider />
							<Grid style={{ margin: '0em' }}>
								<Grid.Column width={4}>Nama Lengkap</Grid.Column><Grid.Column width={12}><b>Zainal Abidin</b></Grid.Column>
								<Grid.Column width={4}>NIK</Grid.Column><Grid.Column width={12}><b></b></Grid.Column>
								<Grid.Column width={4}>Alamat</Grid.Column><Grid.Column width={12}><b>adasafdsf</b></Grid.Column>
								<Grid.Column width={4}>Kota</Grid.Column><Grid.Column width={12}><b></b></Grid.Column>
								<Grid.Column width={4}>Propinsi</Grid.Column><Grid.Column width={12}><b></b></Grid.Column>
							</Grid><br/><br/>
							<h3><Icon name="mail" /> Email & Telepon</h3>
							<Divider />
							<Grid style={{ margin: '0em' }}>
								<Grid.Column width={4}>Email Aktif</Grid.Column><Grid.Column width={12}><b>Zainal Abidin</b></Grid.Column>
								<Grid.Column width={4}>No. Kontak</Grid.Column><Grid.Column width={12}><b></b></Grid.Column>
							</Grid><br/><br/>
							<h3><Icon name="lock" /> Password</h3>
							<Divider />
							<Grid style={{ margin: '0em' }}>
								<Grid.Column width={4}>Ubah Password</Grid.Column><Grid.Column width={12}><b>Zainal Abidin</b></Grid.Column>
								<Grid.Column width={4}>Terakhir Login</Grid.Column><Grid.Column width={12}><b></b></Grid.Column>
							</Grid><br/><br/>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default Profil