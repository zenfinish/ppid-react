import React, { Component } from 'react';
import { Segment, Divider, Form, Dropdown, Button } from 'semantic-ui-react';

class AjukanPermohonan extends Component {
	
	state = {
		loading: false,
		loadingPermohonan: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment raised loading={this.state.loading}>
				<h3>Ajukan Permohonan</h3>
				<Divider />
				<Form>
					<Form.Field>
						<label>Tujuan Permohonan</label>
						<Dropdown
							placeholder='Select Friend'
							fluid
							selection
							options={[
								{
								  key: 'Jenny Hess',
								  text: 'Jenny Hess',
								  value: 'Jenny Hess',
								},
								{
								  key: 'Elliot Fu',
								  text: 'Elliot Fu',
								  value: 'Elliot Fu',
								},]}
						/>
					</Form.Field>
					<Form.Field>
						<label>Judul Dokumen Informasi</label>
						<input placeholder='Masukkan Judul Dokumen Informasi' type="text" onChange={(e) => {this.setState({ email: e.target.value })}} />
					</Form.Field>
					<Form.Field>
						<label>Kandungan Informasi</label>
						<textarea placeholder='Masukkan Kandungan Informasi' type="text" onChange={(e) => {this.setState({ email: e.target.value })}} />
					</Form.Field>
					<Form.Field>
						<label>Tujuan Penggunaan Informasi</label>
						<textarea placeholder='Masukkan Tujuan Penggunaan Informasi' type="text" onChange={(e) => {this.setState({ email: e.target.value })}} />
					</Form.Field>
					<Form.Field>
						<Button loading={this.state.loadingPermohonan}>Proses Permohonan</Button>
					</Form.Field>
				</Form>
			</Segment>
		)
	}
}

export default AjukanPermohonan