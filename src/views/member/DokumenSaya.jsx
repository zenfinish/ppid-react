import React, { Component } from 'react';
import { Segment, Table, Divider, Icon, Button } from 'semantic-ui-react';

class DokumenSaya extends Component {
	
	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment raised loading={this.state.loading}>
				<h3>Dokumen Saya</h3>
				<Divider />
				<Table color="yellow">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell>No.</Table.HeaderCell>
							<Table.HeaderCell>Judul Dokumen</Table.HeaderCell>
							<Table.HeaderCell>Waktu Unduh</Table.HeaderCell>
							<Table.HeaderCell><Icon name="download" /></Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell>Apples</Table.Cell>
							<Table.Cell>200</Table.Cell>
							<Table.Cell>0g</Table.Cell>
							<Table.Cell><Button>Unduh Lagi</Button></Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</Segment>
		)
	}
}

export default DokumenSaya