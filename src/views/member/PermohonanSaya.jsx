import React, { Component } from 'react';
import { Segment, Table, Divider, Icon, Button } from 'semantic-ui-react';

class PermohonanSaya extends Component {
	
	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment raised loading={this.state.loading}>
				<h3>Permohonan Saya</h3>
				<Divider />
				<Table color="yellow">
					<Table.Header>
						<Table.Row>
							<Table.HeaderCell colSpan="4">
								<Button basic color='yellow' onClick={() => {this.setState({ open: true })}}>
									<Icon name='add square' /> Ajukan Permohonan
								</Button>
							</Table.HeaderCell>
						</Table.Row>
						<Table.Row>
							<Table.HeaderCell>No.</Table.HeaderCell>
							<Table.HeaderCell>Tanggal</Table.HeaderCell>
							<Table.HeaderCell>Judul Permohonan</Table.HeaderCell>
							<Table.HeaderCell>Status</Table.HeaderCell>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						<Table.Row>
							<Table.Cell>Apples</Table.Cell>
							<Table.Cell>200</Table.Cell>
							<Table.Cell>0g</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
						<Table.Row>
							<Table.Cell>Orange</Table.Cell>
							<Table.Cell>310</Table.Cell>
							<Table.Cell>0g</Table.Cell>
							<Table.Cell>0g</Table.Cell>
						</Table.Row>
					</Table.Body>
				</Table>
			</Segment>
		)
	}
}

export default PermohonanSaya