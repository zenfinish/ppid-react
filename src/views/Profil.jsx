import React, { Component } from 'react';
import { Segment, Grid, Image } from 'semantic-ui-react';

class Profil extends Component {

	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<div>
							<Image src={require('../img/user.png')} size='mini' circular verticalAlign='middle' style={{ marginRight: 10 }} />
							<span style={{ fontSize: 20 }}>Profil</span>
						</div><br/>
						<Segment raised style={{ padding: '1em' }}>
							<h3>PROFIL</h3>
							<p>SIP PPID adalah Sistem Informasi Publik Pejabat pengelola Informasi dan Dokumentasi atau dapat juga disebut sebagai e-public. SIP PPID atau e-public merupakan aplikasi pengelolaan dan pelayanan informasi untuk PPID yang dikembangkan sesuai Undang-Undang No. 14 Tahun 2008 tentang Keterbukaan Informasi Publik. E-public dirancang dengan platform hybrid - offline dan online, yang terintegrasi antara PPID Pembantu dan PPID Utama dalam sebuah entitas Badan Publik. Informasi lebih lengkap mengenai SIP PPID atau epublic dapat dilihat disini!</p>
							<p>Untuk informasi lebih lanjut dan bagaimana mendapatkan aplikasi ini silahkan ke info@sip-ppid.net</p>
							<h3>TENTANG PPID</h3>
							<p>Kabag Perencanaan, Setditjen Keuangan Daerah, Wisnu Hidayat, kepada Media Keuangan Daerah, di Jakarta, menilai undang-undang tersebut harus disikapi oleh seluruh instansi pemerintah terkait. “Artinya, ketika UU ini sudah diberlakukan maka Badan Publik termasuk Kementerian Dalam Negeri (Kemendagri) dan pemerintah daerah agar mulai melakukan keterbukaan informasi, yang memang diminta oleh publik,” ujur Wisnu. Pasal 7 UU No. 14/2008 mengamatkan bahwa Badan Publik wajib menyediakan, memberikan dan/atau menerbitkan Informasi Publik yang berada di bawah kewenangannya kepada Pemohon Informasi Publik, selain informasi yang dikecualikan sesuai dengan ketentuan. Badan Publik wajib menyediakan Informasi Publik yang akurat, benar, dan tidak menyesatkan.</p>
							<p>Untuk melaksanakan kewajiban tersebut, Badan Publik harus membangun dan mengembangkan sistem informasi dan dokumentasi untuk mengelola Informasi Publik secara baik dan efisien sehingga dapat diakses dengan mudah. Selanjutnya, Badan Publik wajib membuat pertimbangan secara tertulis setiap kebijakan yang diambil untuk memenuhi hak setiap orang atas Informasi Publik. Pertimbangan tersebut antara lain memuat pertimbangan politik, ekonomi, sosial, budaya, dan/atau pertahanan dan keamanan negara. Dalam rangka memenuhi kewajiban tersebut Badan Publik dapat memanfaatkan sarana dan/atau media elektronik dan non elektronik.</p>
							<p>Selain kewajiban tersebut, UU tersebut juga mengamanatkan bahwa setiap Badan Publik wajib mengumumkan Informasi Publik secara berkala, yang meliputi informasi yang terkait dengan Badan Publik; informasi mengenai kegiatan dan kinerja Badan Publik terkait; informasi mengenai laporan keuangan; dan/atau informasi lain yang diatur dalam peraturan perundang-undangan. Kewajiban memberikan dan menyampaikan Informasi Publik dilakukan paling singkat enam bulan sekali.</p>
							<p>Kewajiban menyebarluaskan Informasi Publik disampaikan dengan cara yang mudah dijangkau oleh masyarakat dan dalam bahasa yang mudah dipahami, Cara-cara tersebut ditentukan lebih lanjut oleh Pejabat Pengelola Informasi dan Dokumentasi (PPID) di Badan Publik terkait. Ketentuan lebih lanjut mengenai kewajiban Badan Publik memberikan dan menyampaikan Informasi Publik secara berkala diatur dengan Petunjuk Teknis Komisi Informasi.</p>
							<p>Sementara itu, untuk mewujudkan pelayanan cepat, tepat, dan sederhana setiap Badan Publik menunjuk PPID; dan membuat dan mengembangkan sistem penyediaan layanan informasi secara cepat, mudah, dan wajar sesuai dengan petunjuk teknis  standar layanan Informasi Publik yang berlaku secara nasional. PPID dibantu oleh pejabat fungsional. Sebagai implementasi dari UU No. 14/2008, pemerintah menerbitkan PP No. 61/2010 tentang Pelaksanaan Undang-Undang Nomor 14 Tahun 2008 tentang Keterbukaan Informasi Publik. Sementara itu, sesuai PP No. 61/2010, PPID bertugas dan bertanggungjawab dalam hal, antara lain</p>
							<ul>
								<li>penyediaan, penyimpanan, pendokumentasian, dan pengamanan informasi;</li>
								<li>pelayanan informasi sesuai dengan aturan yang berlaku;</li>
								<li>pelayanan informasi publik yang cepat, tepat, dan sederhana;</li>
								<li>penetapan prosedur operasional penyebarluasan informasi publik; selanjutnya;</li>
								<li>Pengujian konsekuensi;</li>
								<li>Pengklasifikasian informasi dan/atau pengubahannya;</li>
								<li>penetapan informasi yang dikecualikan yang telah habis jangka waktu pengecualiannya sebagai informasi publik yang dapat diakses; dan penetapan pertimbangan tertulis atas setiap kebijakan yang diambil untuk memenuhi hak setiap orang atas informasi publik. Selain ketentuan tersebut, PPID dapat menjalankan tugas dan tanggungjawabnya sesuai dengan ketentuan peraturan perundang-undangan.</li>
							</ul>
							<p>PPID di Kemendagri diketahui oleh Kepala Pusat Penerangan (Kapuspen) Kemendagri. Selain Kapuspen, juga ditetapkan pejabat penghubung pada masing-masing komponen (sekretariat) yang membidangi atau memiliki tanggungjawab terhadap pengelolaan data dan informasi. Khususnya di Ditjen Keuangan Daerah, PPID ditangani oleh Bagian Perencanaan Sesdijen Keuangan Daerah.</p>
							<p>Dalam rangka pengelolaan informasi publik, Presiden juga mengeluarkan Inpres No. 17/2011 tentang Aksi Pencegahan dan Pemberantasan Korupsi Tahun 2012.Inpres tersebut mengamanatkan kepada seluruh Kementerian/Lembaga (K/L) serta pemerintah daerah terkait dengan upaya pencegahan korupsi.</p>
							<p>Dalam rangka pelaksanaan Inpres tersebut, pemerintah telah menyusun rencana aksi nasional. Untuk pemerintah pusat, rencana aksi menjadi domain Kementerian Keuangan (Kemenkeu) terkait dengan transparansi pengelolaan anggaran K/L. Sedangkan untuk transparansi pengelolaan anggaran daerah (TPAD) dilaksanakan oleh Kemendagri. Instruksi tersebut dinilai cukup berat karena baru pertama kali dilakukan oleh Badan Publik, baik di pusat maupun daerah terkait dengan penganggaran. Dalam hal ini, UKP4 meminta Kemendagri untuk menyusun pedoman agar provinsi dan kab/kota menindaklanjuti UU No. 14/2008 serta Inpres No. 17/2011. Kemendagri telah menyelenggarakan rapat dengan  UKP4 untuk mendorong daerah agar lebih transparan terhadap anggaran daerah.</p>
							<p>Diakui, saat ini belum banyak daerah yang menyediakan anggaran untuk mendanai rencana aksi tersebut. Dalam rangka mendorong daerah untuk menyelenggarakan transparasi anggaran, Kemendagri telah mengeluarkan Instruksi Mendagri No. 188.52/1797/SC/2012 tentang Transparasi Pengelolaan Anggaran Daerah (TPAD). Instruksi tersebut ditujukan kepada gubernur seluruh Indonesia dalam rangka pelaksanaan TPAD. Instruksi Mendagri tersebut mengamanatkan pemerintah provinsi untuk menyiapkan menu content dengan nama TPAD dalam website resmi pemerintah provinsi (Pemkot). Pemkot juga perlu mempublikasikan data mutakhir Pemkot pada menu content yang terdiri dari 12 items.</p>
							<p>Selanjutnya, Walikota membuat Instruksi Walikota yang ditujukan kepada satuan kerja untuk menyiapkan menu content dengan nama TPAD dalam website resmi pemerintah kab/kota. Selain itu, Pemkot perlu melaksanakan monitoring dan evaluasi atas pelaksanaan Instruksi Walikota tersebut. Pemkot juga berkoordinasi dengan satuan kerja di wilayah masing-masing agar segara melakukan percepatan bagi daerah yang belum mengimplementasikan Instruksi Walikota serta melaporkan perkembangan data dan menu content TPAD kepada Mendagri. Tahun 2013, UKP4 menetapkan rencana aksi di daerah.</p>
							<p>Saat ini, UKP4 telah menetapkan daerah-daerah sebagai proyek percontohan (pilot project) pelaksanaan TAPD. Sebagai tahap awal, TAPD dilaksanakan di 99 daerah provinsi dan kab/kota, yakni 33 provinsi, 33 kabupaten, dan 33 kota. Dalam hal ini, daerah bertanggungjawab langsung terhadap UKP4 terkait penilaian terhadap rencana aksi daerah. Ditjen Keuangan Daerah berkewajiban membina TPAD pada 99 daerah (provinsi dan kab/kota). Tugas Kemendagri c.q. Ditjen Keuangan Daerah adalah mendorong daerah agar mulai melaksanakan TAPD, termasuk melakukan verifikasi atas rencana aksi yang sudah disepakati oleh UKP4 dan daerah</p>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default Profil