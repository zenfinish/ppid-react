import React, { Component } from 'react';
import { Segment, Grid, Image } from 'semantic-ui-react';

class TugasWewenang extends Component {

	state = {
		loading: false,
	}
	
	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			this.setState({ loading: false });
		});
	}
	
	render() {
		return (
			<Segment basic loading={this.state.loading}>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<div>
							<Image src={require('../img/user.png')} size='mini' circular verticalAlign='middle' style={{ marginRight: 10 }} />
							<span style={{ fontSize: 20 }}>Tugas & Wewenang</span>
						</div><br/>
						<Segment raised style={{ padding: '1em' }}>
							<h3>TUGAS</h3>
							<ul>
								<li>Pengkoordinasikan dan mengkonsolidasikan pengumpulan bahan informasi dan dokumentasi dari PPID pembantu</li>
								<li>Menyimpan, mendokumentasikan, menyediakan dan memberi pelayanan informasi kepada publik</li>
								<li>Melakukan verifikasi bahan informasi publik</li>
								<li>Melakukan uji konsekuensi atas informasi yang dikecualikan</li>
								<li>Melakukan kemutakhiran informasi dan dokumentasi</li>
								<li>Menyediakan informasi dan dokumentasi untuk diakses oleh masyarakat.</li>
							</ul>
							<h3>KEWENANGAN</h3>
							<ul>
								<li>Menolak memberikan informasi yang dikecualikan sesuai dengan ketentuan peraturan perundang-undangan</li>
								<li>Meminta dan memperoleh informasi dari unit kerja/komponen/satuan kerja yang menjadi cakupan kerjanya</li>
								<li>Mengkoordinasikan pemberian pelayanan informasi dengan PPID Pembantu dan/atau Pejabat Fungsional yang menjadi cakupan kerjanya</li>
								<li>Menentukan atau menetapkan suatu informasi dapat/tidaknya diakses oleh publik</li>
								<li>Menugaskan PPID Pembantu dan/atau Pejabat Fungsional untuk membuat, mengumpulkan, serta memelihara informasi dan dokumentasi untuk kebutuhan organisasi.</li>
							</ul>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default TugasWewenang