import React, { Component } from 'react';
import { Segment, Grid } from 'semantic-ui-react';

class Page404 extends Component {
	
	render() {
		return (
			<Segment basic>
				<Grid style={{ padding: 20 }}>
					<Grid.Column width={16}>
						<Segment raised style={{ padding: '1em' }}>
							<h3>404</h3>
							<h1>OOPS!</h1>
							<div>TERDAPAT KESALAHAN ATAU HALAMAN YANG ANDA CARI BELUM TERSEDIA</div>
						</Segment>
					</Grid.Column>
				</Grid>
			</Segment>
		)
	}
}

export default Page404