import React, { Component, Fragment } from 'react';
import { Button, Container, Icon, Menu, Responsive, Segment, Visibility, Image, Dropdown } from 'semantic-ui-react';
import { Link, withRouter } from 'react-router-dom';
import api from './configs/api.js';

const getWidth = () => {
	const isSSR = typeof window === 'undefined'
	return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth
}

class Header extends Component {

	state = {
		active: '',
		token: false,
	}

	componentDidMount() {
		this.setState({
			loading: true
		}, () => {
			api.get(`/people/cek-token`)
			.then(result => {
				this.setState({ token: true });
			})
			.catch(error => {
				this.setState({ token: false }, () => {
					localStorage.removeItem('token');
					this.props.history.push('/');
				});
			});
		});
	}

	logout = () => {
		localStorage.removeItem('token');
		this.props.history.push('/');
		window.location.reload();
	}

	hideFixedMenu = () => this.setState({ fixed: false })
	showFixedMenu = () => this.setState({ fixed: true })
		
	render() {
		const { fixed } = this.state;
		return (
			<Responsive getWidth={getWidth}>
				<Visibility
					once={false}
					onBottomPassed={this.showFixedMenu}
					onBottomPassedReverse={this.hideFixedMenu}
				>
					<Segment
						inverted
						textAlign='center'
						style={{ padding: '1em 0em' }}
						vertical
					>
						<Menu
							size='large'
							fixed={fixed ? 'top' : null}
							inverted={!fixed}
							pointing={!fixed}
							secondary={!fixed}
						>
							<Container>
								
								<Menu.Item
									as={Link}
									to={`${process.env.PUBLIC_URL}/`}
									active={this.state.active === '/' ? true : false}
									onClick={() => {this.setState({ active: '/' })}}
								><Icon name='home' style={{ marginRight: '1em' }} /> Beranda</Menu.Item>

								<Menu.Item
									as={Link}
									to={`${process.env.PUBLIC_URL}/guguk`}
									active={this.state.active === '/guguk' ? true : false}
									onClick={() => {this.setState({ active: '/guguk' })}}
								><Icon name='sitemap' style={{ marginRight: '1em' }} /> Satuan Kerja</Menu.Item>

								<Menu.Item
									as={Link}
									to={`${process.env.PUBLIC_URL}/group`}
									active={this.state.active === '/group' ? true : false}
									onClick={() => {this.setState({ active: '/group' })}}
								><Icon name='group' style={{ marginRight: '1em' }} /> Group</Menu.Item>

								<Menu.Item
									as={Link}
									to={`${process.env.PUBLIC_URL}/permohonan-informasi`}
									active={this.state.active === '/permohonan-informasi' ? true : false}
									onClick={() => {this.setState({ active: '/permohonan-informasi' })}}
								><Icon name='info circle' style={{ marginRight: '1em' }} /> Permohonan Informasi</Menu.Item>

								<Menu.Item position='right' style={{ marginRight: 50 }}>
									{this.state.token === true ?
										<Fragment>
											<Dropdown
												icon={null}
												trigger={<Image src={require('./img/LOGO_KOTA_BANDAR_LAMPUNG_BARU.png')} size="mini" circular />}
											>
												<Dropdown.Menu>
													<Dropdown.Item icon='dashboard' text='Dashboard' as={Link} to="/member/dashboard" />
													<Dropdown.Item icon='user' text='Akun Saya' as={Link} to="/member/profil" />
													<Dropdown.Divider />
													<Dropdown.Item icon='power off' text='Logout' onClick={this.logout} />
												</Dropdown.Menu>
											</Dropdown>
										</Fragment>
									: ''}
									{this.state.token === false ?
										<Button style={{ marginRight: 5 }} as={Link} to={`${process.env.PUBLIC_URL}/login`}>
											Login / Daftar
										</Button>
									: ''}
								</Menu.Item>
							</Container>
						</Menu>
					</Segment>
				</Visibility>
			</Responsive>
		)
	}
}

export default withRouter(Header)